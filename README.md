# Note It

Un proyecto de prueba elaborado a partir de un desarrollo móvil y migrado a web.

Verlo en marcha: [Note It - Demo](https://jonathanhacklife.gitlab.io/note-it/)

## Algunos requisitos antes de empezar

- Tener instalados los plugins de Flutter y Dart actualizados (en mi caso para este momento tengo la versión 35.3.3 para Flutter y la versión 191.7141.49 para Dart).
- Tener instalados los SDK’s de Flutter (versión 1.5) y Dart (versión 2.3).
- Asegurarse de agregar las rutas de los SDK’s de Flutter y Dart en el PATH del sistema operativo en el que se esté desarrollando.

# Construyendo una aplicación web con Flutter

![images/01.gif](images/01.gif)

A modo de ejemplo construiremos una aplicación similar a Google Keep para crear notas rápidas (temporalmente).

# Configurando el entorno

Para comenzar en Flutter, el primer paso es instalarlo. Flutter está disponible para las tres plataformas más populares: **Windows**, **MacOS** y **Linux**. Flutter es muy fácil de instalar, solo hay que seguir la [guía](https://flutter.dev/docs/get-started/install).

Recursos necesarios:

- ***[Visual Studio Code](https://code.visualstudio.com/Download)***
- ***[Flutter SDK](https://flutter.dev/docs/get-started/install)***

Recursos necesarios para **VSCode:**

- **[Dart](https://marketplace.visualstudio.com/items?itemName=Dart-Code.dart-code)**
- **[Flutter](https://marketplace.visualstudio.com/items?itemName=Dart-Code.flutter)** *(v3.19.0)*

Asegurate de estar ejecutando la versión ≥ 3.19.0 de la extensión Flutter.

![images/Untitled.png](images/Untitled.png)

Una vez descargado e instalado, Flutter no está listo para desarrollar para web con la versión estable, así que lo actualizaremos para obtener la última versión que incluye las herramientas necesarias para desarrollar web. Abrimos la terminal y escribimos:

```
flutter upgrade
```

Bien, ahora estamos listos para desarrollar **mobile** y **web**.

# Creando el proyecto

Abrí Visual Studio Code, luego **Ver -> Paleta de comandos** (`Ctrl + Shift + P` o `Command + Shift + P`) y escribí:

```
Flutter: New Web Project
```

![https://miro.medium.com/proxy/1*r5AxF9RL8cOlVp_cW18MuA.png](https://miro.medium.com/proxy/1*r5AxF9RL8cOlVp_cW18MuA.png)

Creá el proyecto con el nombre de “**hello_world**” mismo, podes darle el nombre que prefieras. Espera mientras Flutter descarga todas las políticas y paquetes necesarios para crear el proyecto, este proceso puede tardar un poco la primera vez.

Después de ejecutar el comando, tendremos un proyecto base y lo codificaremos. El proyecto inicial tiene el siguiente esqueleto de archivo:

![images/Untitled%201.png](images/Untitled%201.png)

Utilizá el siguiente comando para habilitar el módulo web para flutter

```
flutter packages pub global activate webdev
```

![https://miro.medium.com/proxy/1*wW72kx5vlAOWWsiB8XMG3g.png](https://miro.medium.com/proxy/1*wW72kx5vlAOWWsiB8XMG3g.png)

Finalmente podemos ver un poco de Flutter en la web, podes iniciar la aplicación usando **F5** o con el comando:

```
flutter packages pub global run webdev serve
```

![https://miro.medium.com/proxy/1*Yel96OU3Wja5hcLX2kNJog.gif](https://miro.medium.com/proxy/1*Yel96OU3Wja5hcLX2kNJog.gif)

Ahora para empezar a codear, abrimos el archivo **main.dart**, ****eliminamos todo y pegamos el siguiente código en el archivo **main.dart**:

```dart
import 'package:flutter_web/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First Application With Flutter Web', // Titulo de la página
      theme: ThemeData(primarySwatch: Colors.blue),
      home: Container(
        child: Center(
          child: CounterWidget(),
        ),
      ),
    );
  }
}

class CounterWidget extends StatefulWidget {
  @override
  _CounterWidgetState createState() => _CounterWidgetState();
}

class _CounterWidgetState extends State<CounterWidget> {
  int _counter = 10;

  Future<void> _mostrarAlerta(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Alerta'),
          content: const Text('Se reiniciará el contador.'),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                setState(() => _counter = 10);
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("$_counter", style: TextStyle(color: Colors.grey, fontSize: 40.0)),
          FlatButton(
            child: Text('Decrementar'),
            color: Colors.amber,
            onPressed: () {
              setState(() {
                _counter--;
                if (_counter == 0) _mostrarAlerta(context);
              });
            },
          )
        ],
      ),
    );
  }
}
```

Ejecutá el código y el resultado será el siguiente:

![https://miro.medium.com/proxy/1*yKxEioT46FznwpjzDXTnaA.gif](https://miro.medium.com/proxy/1*yKxEioT46FznwpjzDXTnaA.gif)

Tenemos una aplicación con control de estados.

# Vayamos a la acción

Ya pudimos ver que la aplicacion funciona exactamente igual que en su versión móvil. Ahora para continuar con nuestra aplicación reemplazaremos todo el contenido por este otro:

[Ir a Main.dart](main.dart)

Ahora crearemos la pantalla para ver las notas en pantalla completa. Para eso debemos crear una carpeta llamada **screen** y dentro un archivo llamado nota.dart

```dart
import 'package:flutter/material.dart';

class NotaScreen extends StatelessWidget {
  NotaScreen(String this.titulo, String this.contenido);
  final titulo, contenido;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: titulo,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () => Navigator.pop(context)),
          title: Text(titulo),
        ),
        body: Column(
          children: [
            Text(contenido, style: Theme.of(context).textTheme.headline5),
          ],
        ),
      ),
    );
  }
}
```

Por último debemos incorporar un patrón BLoC para la transmisión de datos.
En la raíz del proyecto creamos una nueva carpeta llamada **bloc** y dentro un archivo llamado **notaBLoC.dart**

```dart
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class NotaBLoC {
  // Creamos una base de datos local
  List listaDeNotas = [];

  // Creamos los controladores para el Stream de notas
  StreamController _notaController = StreamController();
  Stream get getStreamNotas => _notaController.stream;

  // Algunas funciones CRUD
  void crearNota({String titulo}) {
    Nota nuevaNota = Nota();
    nuevaNota.tituloNota = titulo;
    listaDeNotas.add(nuevaNota);
    _notaController.sink.add(listaDeNotas);
  }

  void modificarTitulo({int indice, String contenido}) {
    listaDeNotas[indice].tituloNota = contenido;
    _notaController.sink.add(listaDeNotas);
  }

  void modificarNota({int indice, String contenido}) {
    listaDeNotas[indice].contenidoNota = contenido;
    _notaController.sink.add(listaDeNotas);
  }

  void borrarNota({int indice}) {
    listaDeNotas.removeAt(indice);
    _notaController.sink.add(listaDeNotas);
  }

  dispose() => _notaController.close();
}

// Clase para crear notas
class Nota {
  String titulo, contenido;

  set tituloNota(String name) => titulo = name;
  String get tituloNota => titulo;

  set contenidoNota(String name) => contenido = name;
  String get contenidoNota => contenido;
}

// [EXPERIMENTAL: Leer y guardar datos en el caché del navegador]
leerDeLaWeb() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  print("Datos de SharedPreferences: ${prefs.getString('data')}");
  var aux = prefs.getString('data');
  print("AUXILIAR: $aux");
  return aux;
}

void guardarEnLaWeb(dato) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('data', dato.toString());
}
```

Ahora ejecutemos el código, presione F5 para ver el resultado. Si está siguiendo el tutorial para rascar, el resultado será como en el siguiente gif:

![images/01.gif](images/01.gif)

Tené en cuenta que nuestro icono es el que viene por defecto con Flutter. Ccorregiremos este problema ahora.
En tu VSCode, creá una carpeta llamada **icon**:

![images/Untitled%202.png](images/Untitled%202.png)

Ahora en nuestro archivo **pubspec.yaml** agregaremos un plugin propio de Flutter llamado **flutter_launcher_icons**.

Lo incorporamos de la siguiente manera:

```yaml
dev_dependencies:
  flutter_test:
    sdk: flutter

  flutter_launcher_icons: "^0.8.1"

flutter_icons:
  image_path: "icon/icon.png"
  android: true
  ios: true
```

Preparamos un icono de aplicación para la ruta especificada. Por ejemplo icon/icon.png
Ejecutamos el comando en la terminal para crear iconos de aplicaciones:

`flutter pub get`
`flutter pub run flutter_launcher_icons:main`

Ahora solo nos queda actualizar nuestro pubspec.yaml y ejecutar.

# Conclusión

Eso es todo, tenemos una aplicación hecha en Web Flutter totalmente utilizable y rápida de crear.

Como Flutter es un framework orientado al desarrollo móvil, es normal que el navegador muestre algunas opciones de Android como el clásico "Copiar" y "Pegar".

**No se recomienda usar Flutter para producción todavía, es mejor esperar hasta que salga la versión más estable de Flutter para web.**

## Limitaciones

A continuación se muestra un listado de limitaciones encontrados antes y después de desarrollar en Flutter Web.

- flutter_web aún no tiene un sistema de complementos. Temporalmente, brindamos acceso a dart: html, dart: js, dart: svg, dart: indexed_db y otras bibliotecas web que le brindan acceso a la gran mayoría de las API del navegador. Sin embargo, se espera que estas bibliotecas sean reemplazadas por una API de complemento diferente.
- No todas las API de Flutter están implementadas en Flutter para web aún.
- El trabajo de rendimiento apenas está comenzando. El código generado por Flutter para web puede ejecutarse lentamente o mostrar un “jank” significativo en la interfaz de usuario.
- En este momento, las interacciones de la interfaz de usuario de escritorio no están completamente completas, por lo que una interfaz de usuario creada con flutter_web puede parecer una aplicación móvil, incluso cuando se ejecuta en un navegador de escritorio.
- El flujo de trabajo de desarrollo solo está diseñado para funcionar con Chrome en este momento.
- Bajo rendimiento: se encuentra en su etapa inicial (infantil), por lo que el rendimiento parece ser bastante bajo.
- No se pueden inspeccionar elementos: esto es en realidad un defecto y una ventaja, como cualquier otra página web, no se pueden inspeccionar los elementos de la aplicación web flutter. Esto es debido a que todo está en lienzo, así que básicamente solo hay un elemento que tiene nuestra aplicación completa.
- El enrutamiento necesita más trabajo: el enrutamiento es una de las características más importantes en cualquier aplicación y si esto no funciona bien, el marco no está ni cerca de estar listo.

### Soporte del navegador

Flutter para web proporciona:

- Un compilador de producción de JavaScript que genera código optimizado y minimizado para la implementación
- Un compilador de desarrollo de JavaScript, que ofrece compilación incremental y hot restart
- Cuando se construyó con el compilador de producción, Flutter admite navegadores basados en Chromium y Safari, tanto en computadoras de escritorio como móviles. También pretendemos ser totalmente compatibles con Firefox y Edge como plataformas específicas, pero nuestra propia cobertura de prueba aún es baja en estos navegadores. Nuestra intención es apoyar la versión actual y las dos últimas publicaciones principales. Se agradece la retroalimentación sobre problemas de rendimiento y rendimiento en todos estos navegadores.
- El soporte de Internet Explorer no está planeado.
- El compilador de desarrollo actualmente solo admite Chrome.Muestras

Acá unos ejemplos elaborados por Flutter: [flutter.github.io/samples](http://flutter.github.io/samples).

## Recursos:

[Web support for Flutter](https://flutter.dev/web)

[Build and release a web app](https://flutter.dev/docs/deployment/web)

[The Art of Flutter Web](https://medium.com/fluttersg/the-art-of-flutter-flutter-web-383d5db568a0)

[Guía de migración a Flutter Web](https://github.com/flutter/flutter_web/blob/master/docs/migration_guide.md)