﻿import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class NotaBLoC {
  // Creamos una base de datos local
  List listaDeNotas = [];

  // Creamos los controladores para el Stream de notas
  StreamController _notaController = StreamController();
  Stream get getStreamNotas => _notaController.stream;

  // Algunas funciones CRUD
  void crearNota({String titulo}) {
    Nota nuevaNota = Nota();
    nuevaNota.tituloNota = titulo;
    listaDeNotas.add(nuevaNota);
    _notaController.sink.add(listaDeNotas);
  }

  void modificarTitulo({int indice, String contenido}) {
    listaDeNotas[indice].tituloNota = contenido;
    _notaController.sink.add(listaDeNotas);
  }

  void modificarNota({int indice, String contenido}) {
    listaDeNotas[indice].contenidoNota = contenido;
    _notaController.sink.add(listaDeNotas);
  }

  void borrarNota({int indice}) {
    listaDeNotas.removeAt(indice);
    _notaController.sink.add(listaDeNotas);
  }

  dispose() => _notaController.close();
}

// Clase para crear notas
class Nota {
  String titulo, contenido;

  set tituloNota(String name) => titulo = name;
  String get tituloNota => titulo;

  set contenidoNota(String name) => contenido = name;
  String get contenidoNota => contenido;
}

// [EXPERIMENTAL: Leer y guardar datos en el caché del navegador]
leerDeLaWeb() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  print("Datos de SharedPreferences: ${prefs.getString('data')}");
  var aux = prefs.getString('data');
  print("AUXILIAR: $aux");
  return aux;
}

void guardarEnLaWeb(dato) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('data', dato.toString());
}
