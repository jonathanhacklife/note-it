﻿import 'package:flutter/material.dart';

class NotaScreen extends StatelessWidget {
  NotaScreen(String this.titulo, String this.contenido);
  final titulo, contenido;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: titulo,
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(onPressed: () => Navigator.pop(context)),
          title: Text(titulo),
        ),
        body: Column(
          children: [
            Text(contenido, style: Theme.of(context).textTheme.headline5),
          ],
        ),
      ),
    );
  }
}
