import 'package:flutter/material.dart';
import 'screens/nota.dart';
import 'bloc/notaBLoC.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Note It - Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Creamos controladores para las alertas de Título y Mensaje
  TextEditingController titleFieldController = TextEditingController();
  TextEditingController contentFieldController = TextEditingController();
  NotaBLoC blocData = NotaBLoC();

  // Alerta de título
  _dialogoTitulo(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Título de la nota'),
          content: TextFormField(
            controller: titleFieldController,
            decoration: InputDecoration(hintText: "Ingrese un título"),
            autofocus: true,
            onFieldSubmitted: (value) {
              setState(() {
                if (value.isNotEmpty) {
                  Navigator.pop(context, value);
                  titleFieldController.text = "";
                }
              });
            },
          ),
          actions: <Widget>[
            TextButton(
              child: Text('ACEPTAR'),
              onPressed: () => setState(() {
                if (titleFieldController.text.isNotEmpty) {
                  Navigator.pop(context, titleFieldController.text);
                  titleFieldController.text = "";
                }
              }),
            ),
          ],
        );
      },
    );
  }

  // Alerta de Mensaje
  _dialogoContenido(BuildContext context) {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Mensaje de la nota'),
          content: TextField(
            controller: contentFieldController,
            decoration: InputDecoration(hintText: "Ingrese un mensaje"),
            autofocus: true,
            onSubmitted: (value) => setState(() {
              if (value.isNotEmpty) {
                Navigator.pop(context, contentFieldController.text);
                contentFieldController.text = "";
              }
            }),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('ACEPTAR'),
              onPressed: () => setState(() {
                if (contentFieldController.text.isNotEmpty) {
                  Navigator.pop(context, contentFieldController.text);
                  contentFieldController.text = "";
                }
              }),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Obtenemos las dimensiones de la pantalla para trabajar el diseño responsive
    bool isTablet = MediaQuery.of(context).size.width > 660 && MediaQuery.of(context).size.height > 375;
    bool isComputer = MediaQuery.of(context).size.width > 1200 && MediaQuery.of(context).size.height > 720;

    return Scaffold(
      appBar: AppBar(title: Text("Note It")),
      body: StreamBuilder(
        initialData: (leerDeLaWeb is List) ? leerDeLaWeb() : [],
        stream: blocData.getStreamNotas,
        builder: (context, snapshot) {
          /*Si nuestro Stream tiene datos, guardará esos valores en el LocalStorage
            para que persistan con el tiempo.*/
          if (snapshot.hasData) {
            print("Datos de la web: ${leerDeLaWeb()}");
            guardarEnLaWeb(snapshot.data);
            // Se creará una grilla en base a las dimensiones del dispositivo en el que se ejecute.
            return GridView.count(
              crossAxisCount: isComputer
                  ? 6
                  : isTablet
                      ? 4
                      : 2,
              children: List.generate(
                snapshot.data.length,
                (index) {
                  // Card con información de las notas
                  return Card(
                    color: Colors.blue,
                    margin: EdgeInsets.all(5),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    // Título de la nota
                                    child: Text(
                                      '${snapshot.data[index].tituloNota}',
                                      style: Theme.of(context).textTheme.headline5,
                                      textAlign: TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  botonEditarTitulo(context, index),
                                ],
                              ),
                              Divider(color: Colors.white),
                              // Descripción de la nota
                              if (snapshot.data[index].contenidoNota != null)
                                Row(
                                  children: [
                                    Expanded(
                                      child: TextButton(
                                        child: Expanded(
                                          child: Text(
                                            '${snapshot.data[index].contenidoNota}',
                                            style: Theme.of(context).textTheme.bodyText1,
                                            textAlign: TextAlign.left,
                                            overflow: TextOverflow.ellipsis,
                                            // Se mostrará el máximo de líneas segun las dimensiones del dispositivo.
                                            maxLines: isComputer
                                                ? 8
                                                : isTablet
                                                    ? 3
                                                    : 5,
                                          ),
                                        ),
                                        // Navegamos a la siguiente pantalla (Para ver el contenido de la nota)
                                        onPressed: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => NotaScreen('${snapshot.data[index].tituloNota}', '${snapshot.data[index].contenidoNota}')),
                                        ),
                                      ),
                                    ),
                                    botonEditarDescripcion(context, index),
                                  ],
                                )
                              else
                                // En el caso de que la nota no tenga descripción, se mostrará un botón para agregarla.
                                Row(
                                  children: [
                                    Expanded(child: botonAgregarDescripcion(context, index)),
                                  ],
                                ),
                            ],
                          ),
                          Row(children: [Expanded(child: botonEliminar(index))])
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            // En el caso de que no hayan notas activas. Se mostrará el siguiente aviso.
            return Center(
              child: Text(
                "No tiene notas :(\n\nCree una nueva nota presionando el botón '+'.",
                style: Theme.of(context).textTheme.headline4,
                textAlign: TextAlign.center,
              ),
            );
          }
        },
      ),
      floatingActionButton: botonCrearNota(context),
    );
  }

// SECCIÓN DE BOTONES (WIDGETS)
  FloatingActionButton botonCrearNota(BuildContext context) {
    return FloatingActionButton(
      child: Icon(Icons.add),
      tooltip: 'Crear nota',
      onPressed: () => _dialogoTitulo(context).then((valor) {
        if (valor != null) blocData.crearNota(titulo: valor);
      }),
    );
  }

  FloatingActionButton botonEditarTitulo(BuildContext context, int index) {
    return FloatingActionButton(
      child: Icon(Icons.mode_edit, color: Colors.black),
      tooltip: "Editar título",
      backgroundColor: Colors.white,
      mini: true,
      onPressed: () => _dialogoTitulo(context).then(
        (valor) => blocData.modificarTitulo(indice: index, contenido: valor),
      ),
    );
  }

  MaterialButton botonAgregarDescripcion(BuildContext context, int index) {
    return MaterialButton(
      child: Icon(Icons.add),
      color: Colors.white,
      padding: EdgeInsets.all(0),
      onPressed: () => _dialogoContenido(context).then(
        (valor) => blocData.modificarNota(indice: index, contenido: valor),
      ),
    );
  }

  FloatingActionButton botonEditarDescripcion(BuildContext context, int index) {
    return FloatingActionButton(
      child: Icon(Icons.mode_edit, color: Colors.black),
      tooltip: "Agregar descripción",
      backgroundColor: Colors.white,
      mini: true,
      onPressed: () => _dialogoContenido(context).then(
        (valor) => blocData.modificarNota(indice: index, contenido: valor),
      ),
    );
  }

  MaterialButton botonEliminar(int index) {
    return MaterialButton(
      child: Icon(Icons.delete_forever_outlined),
      color: Colors.red,
      padding: EdgeInsets.all(0),
      onPressed: () => setState(() => blocData.borrarNota(indice: index)),
    );
  }

  void dispose() {
    titleFieldController.dispose();
    contentFieldController.dispose();
    super.dispose();
  }
}
